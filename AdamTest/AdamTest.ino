#include <ESP8266WiFi.h>
#include "MyOneWire.h"
#include "PrivateVars.h"
#include <Wire.h>
#include "SparkFunHTU21D.h"

const char* host = "data.sparkfun.com";
const char* streamId   = "pwvvR0JGNDtL5O2zXgGZ";
HTU21D myHumidity;

OneWire  ds(2);  // on pin 2 (a 4.7K resistor is necessary)


void setup(void) {
  //Serial.begin(9600);
  Wire.pins(2, 0);
  myHumidity.begin();
  delay(100);

  //Serial.println();
  //Serial.println();
  //Serial.print("Connecting to: ");
  //Serial.print(ssid);
  
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    //Serial.print(".");
  }

  Serial.print("WiFi connected!");  
  Serial.print("\nIP address: ");
  Serial.println(WiFi.localIP());
}

void loop(void) {
  byte i;
  byte present = 0;
  byte data[12];
  byte addr[8] = {0x28,0xC2,0xD4,0x80,0x06,0x00,0x00,0xA6};
  float celsius, fahrenheit;
  float humd = myHumidity.readHumidity();

  //Serial.print("connecting to ");
  //Serial.println(host);
  // Use WiFiClient class to create TCP connections
  WiFiClient client;
  const int httpPort = 80;
  if (!client.connect(host, httpPort)) {
    //Serial.println("connection failed");
    return;
  }

  
  ds.reset();
  ds.select(addr);
  ds.write(0x44);        // start conversion
  
  delay(1000);     // maybe 750ms is enough, maybe not
  // we might do a ds.depower() here, but the reset will take care of it.
  
  present = ds.reset();
  ds.select(addr);    
  ds.write(0xBE);         // Read Scratchpad

  for ( i = 0; i < 9; i++) {           // we need 9 bytes
    data[i] = ds.read();
  }


  // Convert the data to actual temperature
  // because the result is a 16 bit signed integer, it should
  // be stored to an "int16_t" type, which is always 16 bits
  // even when compiled on a 32 bit processor.
  int16_t raw = (data[1] << 8) | data[0];
  

  celsius = (float)raw / 16.0;
  fahrenheit = celsius * 1.8 + 32.0;
  //Serial.print("\nTemperature = ");
  //Serial.print(celsius, 4);
  //Serial.print(" Celsius, ");
  //Serial.print(fahrenheit, 6);
  //Serial.println(" Fahrenheit\n");
  // We now create a URI for the request
  String url = "/input/";
  url += streamId;
  url += "?private_key=";
  url += privateKey;
  url += "&temp=";
  url += String(fahrenheit,6);
  url += "&humidity=";
  url += String(humd,6);
  
  //Serial.print("Requesting URL: ");
  //Serial.println(url);
  //Serial.println("");
  
  // This will send the request to the server
  client.print(String("GET ") + url + " HTTP/1.1\r\n" +
               "Host: " + host + "\r\n" + 
               "Connection: close\r\n\r\n");
  delay(10);
  
  // Read all the lines of the reply from server and print them to Serial
  while(client.available()){
    String line = client.readStringUntil('\r');
    //Serial.print(line);
  }
  //Serial.println();
  //Serial.println("closing connection");
  ESP.deepSleep(10000000, WAKE_RF_DEFAULT);
}
