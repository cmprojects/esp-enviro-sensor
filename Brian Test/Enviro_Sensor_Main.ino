/*
*  8/16/15
*/

#include <Wire.h>
#include <ESP8266WiFi.h>
#include <Esp.h>
#include "Adafruit_MCP9808.h"
#include "Adafruit_HTU21DF.h"
#include "Adafruit_BMP085.h"
#include "PrivateVars.h"

// Create the sensor objects
Adafruit_MCP9808 temperatureSensor = Adafruit_MCP9808();
Adafruit_HTU21DF humiditySensor = Adafruit_HTU21DF();
Adafruit_BMP085 pressureSensor;

// I like port 8080
WiFiServer server(8080);

void setup() {
  // Set up I2C on pins 4 and 5, SDA and SCL respectively
  Wire.pins(4, 5);
  // Take a roll call of all the sensors, this will be improved so it can
  // still run if a sensor is down
  if (!temperatureSensor.begin()) {
    while (1);
  }
  if (!humiditySensor.begin()) {
    while (1);
  }
  if (!pressureSensor.begin()) {
    while (1);
  }

  // Attempt to connect to WiFi
  WiFi.mode(WIFI_STA); // disable ESP's Access Point
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
  }
  // Fire up that TCP server on port 8080
  server.begin();
}

void loop() {
  String req; // request from GET
  String res; // response to Pi
  
  WiFiClient client = server.available();
  if (!client) {
    return;
  }
  // Wait until the client sends some data
  while (!client.available()) {
    delay(1);
  }
  // Read the stuff after the question mark, AKA query string
  if (client.find("/?")) {
    req = client.readStringUntil('H'); // I forgot why I use 'H' here, but it works
  }
  client.flush();

  // Code to process the Query String
  // can use "string.indexOf" to find anything in the Query String
  if (req.indexOf("temp") != -1) {
    res = readTPH();
  }

  // Prepare the response to the Pi
  String s = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n";
  s += res; 
  s += "\n";
  // Send the response to the Pi
  client.print(s);
  delay(1);
  
  // just messing with deep sleep. 72mA down to 150uA with all sensors connected, 120uA without sensors
  //ESP.deepSleep(60000000, WAKE_RF_DEFAULT);
}

String readTPH(){
    // sensor read routine
    String resp;
    temperatureSensor.shutdown_wake(0); // wake temp sensor
    float tempC = temperatureSensor.readTempC();
    float tempF = tempC * 9.0 / 5.0 + 32;
    float humidity = humiditySensor.readHumidity();
    float pressure = pressureSensor.readPressure();
    // added heat index to humidity sensor library, taken from DHT library
    float heatIndex = humiditySensor.computeHeatIndex(tempF, humidity);
    
    resp = tempF;
    resp += " ";
    resp += humidity;
    resp += " ";
    resp += pressure;
    resp += " ";
    resp += heatIndex;
    
    delay(500); // anti chaos delay
    
    temperatureSensor.shutdown_wake(1); // sleep temp sensor, others only draw ~18uA
    return resp;
}

